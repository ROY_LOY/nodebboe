ARG NODE_VERSION=12-alpine
FROM node:${NODE_VERSION}
LABEL maintainer="shane@northernv.com"
LABEL org.opencontainers.image.source="https://gitlab.com/northernv/dockerfiles/node.git"

RUN apk update && \
    apk upgrade && \
    apk add --no-cache curl jq bash git postgresql coreutils dumb-init

RUN npm install -g --unsafe-perm=true netlify-cli nodemon

EXPOSE 80 443
